module.exports = {
    formatDDMMAA_HHMM: function (sDate) {
        var hourDate = sDate.split('T');
        return hourDate[0].substr(8) + '/' + hourDate[0].substr(5, 2) + '/' + hourDate[0].substr(0, 4) +
            " " + hourDate[1].substr(0,2) + ":" + hourDate[1].substr(3,2);
    }
};