const ctrlLogin = require('../controllers/loginController.js');
const ctrlCuentas = require('../controllers/cuentasController.js');
const ctrlTransfProp = require('../controllers/transferenciasPropiasController.js');
const ctrlUsuarios = require('../controllers/usuariosController');

module.exports = function(app) {

    // Servicios privados validados con token
    app.get('/api/secure/v1/listadoCuentas', ctrlCuentas.listadoCuentas);
    app.get('/api/secure/v1/listadoMovimientos/:numeroCuenta', ctrlCuentas.listadoMovimientos);

    app.post('/api/secure/v1/transferenciasPropias/realizacion', ctrlTransfProp.realizacionMovimiento);

    app.get('/api/secure/v1/usuarios/:codigoUsuario', ctrlUsuarios.getUser);
    app.put('/api/secure/v1/usuarios/:codigoUsuario', ctrlUsuarios.updateUser);

    //Servicios públicos
    app.post('/api/v1/login', ctrlLogin.login);
};