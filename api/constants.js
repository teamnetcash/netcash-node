module.exports = {
	MONEDA : {
        USD : "Dólares",
        PEN : "Soles"
    },
    TIPO_CUENTA: {
        "1001": "MUNDO SUELDO",
        "1002": "PLAZO FIJO",
        "1003": "AHORROS"
    },
    SERVICIOS_ASOCIADOS: {
        "3000": "TRANSFERENCIAS A CUENTAS PROPIAS",
        "3001": "TRANSFERENCIAS A CUENTAS DE TERCEROS",
        "3002": "TRANSFERENCIAS A CUENTAS INTERBANCARIAS"
    }
};