var configuration = require('../../config');
var requestJson = require('request-json');
var stringUtils = require('../utils/stringUtils');
var dateUtils = require('../utils/dateUtils');
var constantes = require('../constants');

let controller = {
	getUser: function (req, res) {
		var clienteMlab = requestJson.createClient(configuration.mlab.URL_EMPRESAS + configuration.mlab.API_KEY +
			'&q={"_id":{$oid:"' + req.dataToken._id + '"}}' +
			'&fo=true'
		);

		clienteMlab.get("", function (err, resM, bodyMlab) {
			if (err) {
				console.log(err);
			} else {
				res.setHeader('Content-Type', 'application/json');
				var usuario = bodyMlab.empleados.find(function(element) {
                    if (req.params.codigoUsuario == element.codigo) {
                        return element;
                    }
                });
                console.log(usuario);
                res.send(usuario);
			}
		});
    },
    
    updateUser: function (req, res) {
		var clienteMlab = requestJson.createClient(configuration.mlab.URL_EMPRESAS + configuration.mlab.API_KEY +
			'&q={"_id":{$oid:"' + req.dataToken._id + '"}}' +
			'&fo=true'
		);

		clienteMlab.get("", function (err, resM, bodyMlab) {
			if (err) {
				console.log(err);
			} else {
				res.setHeader('Content-Type', 'application/json');
				
                var usuario = bodyMlab.empleados.find(function(element) {
                    if (req.params.codigoUsuario == element.codigo) {
                        return element;
                    }
                });

                usuario.codigo = req.body.codigoUsuario;
                usuario.celular = req.body.numeroCelular;
                usuario.email = req.body.correoElectronico;
                usuario.tipoEmpleado = req.body.tipoUsuarioSeleccionado;

                console.log(usuario);
                
                // actualizar mlab
                clienteMlab.put(configuration.mlab.URL_EMPRESAS + configuration.mlab.API_KEY +
                    '&q={"_id":{$oid:"' + req.dataToken._id + '"}}',
                    {"$set":{"empleados": bodyMlab.empleados}}, 
                    function (err, resM, responseUpdateMlab) {
                        console.log(responseUpdateMlab);
                        if (err) {
                            console.log(err);
                            res.status(500).send({
                                message: 'Hubo un error en la transaccion'
                            });
                        } else {
                            res.send({
                                message: 'Usuario Actualizado'
                            });
                        }
                });
			}
		});
	}
};
module.exports = controller;