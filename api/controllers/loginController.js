var jwt = require('jsonwebtoken');
var configuration = require('../../config');
var requestJson = require('request-json');

let controller = {
    login: function (req, res) {
        var referencia = req.body.referencia;
        var username = req.body.username;
        var password = req.body.password;
        console.log(req.body);

        // 1. Validamos las credenciales de acceso
        var clienteMlab = requestJson.createClient(configuration.mlab.URL_EMPRESAS + configuration.mlab.API_KEY +
            '&q={referencia:"' + referencia + '",empleados:{$elemMatch:{codigo:"' + username + '"}}}' + 
            '&f={referencia:1,empleados:1}');
        clienteMlab.get("", function (err, resM, body) {
            if (err) {
                console.log(err);
            } else if (body.length > 0) {
                
                // 2. Creamos los datos a tokenizar
                var tokenData = {
                    username: referencia + username,
                    _id: body[0]._id.$oid
                };
                console.log("OBJETO DE SEGURIDAD JWT = " + JSON.stringify(tokenData));

                // JWT: Pasamos como parametro el json a tokenizar y la clave secreta que esta en el config
                var token = jwt.sign(tokenData, configuration.jwt.secret, {
                    expiresIn: 60 * configuration.server.TIME_SESSION // Expira en 10 min
                });

                console.log("CODIGO DE SEGURIDAD JWT = " + token);


                var usuarioLogueado = body[0].empleados.find(function(element) {
                    if (element.codigo == username) {
                        return element;
                    }
                });

                var esUsuariosSolidario = "solidario" == usuarioLogueado.tipoEmpleado;

                res.send({
                    token,
                    esUsuariosSolidario
                });
            } else {
                console.error("no se encontro");
                res.status(401).send({
                    message: 'Empresa o Usuario no encontrados'
                });
            }
        });
    }
};
module.exports = controller;