var configuration = require('../../config');
var requestJson = require('request-json');
var stringUtils = require('../utils/stringUtils');
var dateUtils = require('../utils/dateUtils');
var constantes = require('../constants');

let controller = {
	realizacionMovimiento: function (req, res) {
		var clienteMlab = requestJson.createClient(configuration.mlab.URL_EMPRESAS + configuration.mlab.API_KEY +
			'&q={"_id":{$oid:"' + req.dataToken._id + '"}}' +
			'&fo=true'
		);

		var clienteTipoCambio = requestJson.createClient(configuration.URL_TIPO_CAMBIO);
		clienteMlab.get("", function (err, resM, bodyMlab) {
			if (err) {
				console.log(err);
			} else {
				res.setHeader('Content-Type', 'application/json');
				var cuentaCargo = bodyMlab.cuentas.find(function(element) {
					if (element.numero == req.body.cuentaCargo) {
						return element;
					}
				});
				var cuentaAbono = bodyMlab.cuentas.find(function(element) {
					if (element.numero == req.body.cuentaAbono) {
						return element;
					}
				});
				var tipoCambio = 0;

				// Verificamos si requiere tipo de cambio
				if (cuentaAbono.divisa != cuentaCargo.divisa) {
					clienteTipoCambio.get("", function(err, resM, body) {
						tipoCambio = body.Cotizacion[0].Venta;

						if (req.body.divisaImporte == "PEN") {
							req.body.importePEN = req.body.importe;
							req.body.importeUSD = req.body.importe / tipoCambio;
						} else if (req.body.divisaImporte == "USD") {
							req.body.importePEN = req.body.importe * tipoCambio;
							req.body.importeUSD = req.body.importe;
						}

						if (cuentaCargo.divisa == "PEN") {
							// Restamos a la cuenta de cargo el importe
							cuentaCargo.saldoDisponible = cuentaCargo.saldoDisponible - req.body.importePEN;
							cuentaCargo.saldoContable = cuentaCargo.saldoContable - req.body.importePEN;
							
							// Agregamos el importe a la cuenta de abono
							cuentaAbono.saldoDisponible = cuentaAbono.saldoDisponible + req.body.importeUSD;
							cuentaAbono.saldoContable = cuentaAbono.saldoContable + req.body.importeUSD;

						} else if (cuentaCargo.divisa == "USD") {
							// Restamos a la cuenta de cargo el importe
							cuentaCargo.saldoDisponible = cuentaCargo.saldoDisponible - req.body.importeUSD;
							cuentaCargo.saldoContable = cuentaCargo.saldoContable - req.body.importeUSD;

							// Agregamos el importe a la cuenta de abono
							cuentaAbono.saldoDisponible = cuentaAbono.saldoDisponible + req.body.importePEN;
							cuentaAbono.saldoContable = cuentaAbono.saldoContable + req.body.importePEN;
						}

						console.log(req.body);
						var nuevoMovimiento = {
							monto: - req.body.importe,
							fecha: new Date(),
							descripcion: constantes.SERVICIOS_ASOCIADOS[3000],
							divisaMovimiento: req.body.divisaImporte,
							comentarios: req.body.referencia
						}

						var nuevoMovimientoAbono = {
							monto: req.body.importe,
							fecha: new Date(),
							descripcion: constantes.SERVICIOS_ASOCIADOS[3000],
							divisaMovimiento: req.body.divisaImporte,
							comentarios: req.body.referencia
						}
	
						cuentaAbono.movimientos.push(nuevoMovimientoAbono);
						cuentaCargo.movimientos.push(nuevoMovimiento);

						// actualizar mlab
						clienteMlab.put(configuration.mlab.URL_EMPRESAS + configuration.mlab.API_KEY +
							'&q={"_id":{$oid:"' + req.dataToken._id + '"}}',
							{"$set":{"cuentas": bodyMlab.cuentas}}, 
							function (err, resM, responseUpdateMlab) {
								console.log(responseUpdateMlab);
								if (err) {
									console.log(err);
									res.status(500).send({
										message: 'Hubo un error en la transaccion'
									});
								} else {
									if (cuentaAbono.divisa != cuentaCargo.divisa) {
										nuevoMovimiento.tipoCambio = tipoCambio;
									}
									nuevoMovimiento.cuentaCargo = cuentaCargo.numero + " " + constantes.MONEDA[cuentaCargo.divisa];
									nuevoMovimiento.cuentaAbono = cuentaAbono.numero + " " + constantes.MONEDA[cuentaAbono.divisa];
									nuevoMovimiento.divisaMovimiento = constantes.MONEDA[nuevoMovimiento.divisaMovimiento];
									nuevoMovimiento.fecha = dateUtils.formatDDMMAA_HHMM(nuevoMovimiento.fecha.toISOString());
									nuevoMovimiento.monto = stringUtils.formatMoney(- nuevoMovimiento.monto);
				
									res.send(nuevoMovimiento);
								}
						});
					});
				} else {
					// Restamos a la cuenta de cargo el importe
					cuentaCargo.saldoDisponible = cuentaCargo.saldoDisponible - req.body.importe;
					cuentaCargo.saldoContable = cuentaCargo.saldoContable - req.body.importe;

					// Agregamos el importe a la cuenta de abono
					cuentaAbono.saldoDisponible = cuentaAbono.saldoDisponible + req.body.importe;
					cuentaAbono.saldoContable = cuentaAbono.saldoContable + req.body.importe;

					// Creamos el nuevo movimiento para cada cuenta
					var nuevoMovimiento = {
						monto: - req.body.importe,
						fecha: new Date(),
						descripcion: constantes.SERVICIOS_ASOCIADOS[3000],
						divisaMovimiento: req.body.divisaImporte,
						comentarios: req.body.referencia
					}

					var nuevoMovimientoAbono = {
						monto: req.body.importe,
						fecha: new Date(),
						descripcion: constantes.SERVICIOS_ASOCIADOS[3000],
						divisaMovimiento: req.body.divisaImporte,
						comentarios: req.body.referencia
					}

					cuentaAbono.movimientos.push(nuevoMovimiento);
					cuentaCargo.movimientos.push(nuevoMovimientoAbono);

					// actualizar mlab
					clienteMlab.put(configuration.mlab.URL_EMPRESAS + configuration.mlab.API_KEY +
						'&q={"_id":{$oid:"' + req.dataToken._id + '"}}',
						{"$set":{"cuentas": bodyMlab.cuentas}}, 
						function (err, resM, responseUpdateMlab) {
							console.log(responseUpdateMlab);
							if (err) {
								console.log(err);
								res.status(500).send({
									message: 'Hubo un error en la transaccion'
								});
							} else {
								nuevoMovimiento.cuentaCargo = cuentaCargo.numero + " " + constantes.MONEDA[cuentaCargo.divisa];
								nuevoMovimiento.cuentaAbono = cuentaAbono.numero + " " + constantes.MONEDA[cuentaAbono.divisa];
								nuevoMovimiento.divisaMovimiento = constantes.MONEDA[nuevoMovimiento.divisaMovimiento];
								nuevoMovimiento.fecha = dateUtils.formatDDMMAA_HHMM(nuevoMovimiento.fecha.toISOString());
								nuevoMovimiento.monto = stringUtils.formatMoney(- nuevoMovimiento.monto)

								res.send(nuevoMovimiento);
							}
					});

				}
			}
		});
	}
};
module.exports = controller;