var configuration = require('../../config');
var requestJson = require('request-json');

let controller = {
	listadoCuentas: function (req, res) {
		var clienteMlab = requestJson.createClient(configuration.mlab.URL_EMPRESAS + configuration.mlab.API_KEY);
		clienteMlab.get("", function (err, resM, body) {
			if (err) {
				console.log(err);
			} else {
				res.setHeader('Content-Type', 'application/json');
				res.send(body);
			}
		});
	}
};
module.exports = controller;