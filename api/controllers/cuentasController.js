var configuration = require('../../config');
var requestJson = require('request-json');
var stringUtils = require('../utils/stringUtils');
var dateUtils = require('../utils/dateUtils');
var constantes = require('../constants');

let controller = {
	listadoCuentas: function (req, res) {
		var filtroPorServicioAsociado = "";
		
		// verificamos si se esta consultado las cuentas por servicio asociado
		if (req.query["servicioAsociado"]) {
			filtroPorServicioAsociado = ',empleados:1';
		}

		var clienteMlab = requestJson.createClient(configuration.mlab.URL_EMPRESAS + configuration.mlab.API_KEY +
			'&q={"_id":{$oid:"' + req.dataToken._id + '"}}' +
			'&fo=true' + 
			'&f={cuentas:1,cuentas.numero:1,cuentas.saldoContable:1,cuentas.saldoDisponible:1,cuentas.divisa:1,cuentas.tipoCuenta:1' + filtroPorServicioAsociado + '}'
		);

		clienteMlab.get("", function (err, resM, body) {
			if (err) {
				console.log(err);
			} else {
				body.cuentas.forEach(element => {
					element.saldoDisponible = stringUtils.formatMoney(element.saldoDisponible);
					element.saldoContable = stringUtils.formatMoney(element.saldoContable);
					element.divisa = constantes.MONEDA[element.divisa];
					element.tipoCuenta = constantes.TIPO_CUENTA[element.tipoCuenta];
				});

				// logica para filtrar las cuentas por servicio asociado del usuario conectado
				if (req.query["servicioAsociado"]) {
					var empleado = body.empleados.find(function(element) {
						var usuarioEnToken = req.dataToken.username.substr(8,15);// obtenemos el usuario almacenado del token
						if (element.codigo == usuarioEnToken) {
							return element;
						}
					});

					var listaCuentasHabilitadas = [];
					empleado.serviciosAsociados.find(function(element) {
						console.log(element.codigo);
						if (req.query["servicioAsociado"] == element.codigo) {
							listaCuentasHabilitadas = element.cuentasAsociadas;
						}
					});

					listadoCuentasPorServicio = [];
					body.cuentas.forEach(element => {
						listaCuentasHabilitadas.forEach(cuentaHabilitada => {
							if (element.numero == cuentaHabilitada.numCuenta) {
								listadoCuentasPorServicio.push(element);
							}
						});
					});
					body.cuentas = listadoCuentasPorServicio;
				}

				res.setHeader('Content-Type', 'application/json');
				res.send(body.cuentas);
			}
		});
	},

	listadoMovimientos: function (req, res) {
		console.log(req.params.numeroCuenta);
		var clienteMlab = requestJson.createClient(configuration.mlab.URL_EMPRESAS + configuration.mlab.API_KEY +
			'&q={"_id":{$oid:"' + req.dataToken._id + '"}}' +
			'&fo=true' + 
			'&f={cuentas:1,cuentas.movimientos:1,cuentas.numero:1}'
		);
		clienteMlab.get("", function (err, resM, body) {
			if (err) {
				console.log(err);
			} else {
				res.setHeader('Content-Type', 'application/json');
				var listaCuentasMovimientos = body.cuentas.find(function(element) {
					if (element.numero == req.params.numeroCuenta) {
						return element;
					}
				});
				listaCuentasMovimientos.movimientos.forEach(element => {
					element.monto = stringUtils.formatMoney(element.monto);
					element.divisaMovimiento = constantes.MONEDA[element.divisaMovimiento];
					element.fecha = dateUtils.formatDDMMAA_HHMM(element.fecha);
				});
				res.send(listaCuentasMovimientos.movimientos);
			}
		});
	}
};
module.exports = controller;