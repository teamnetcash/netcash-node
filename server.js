var jwt = require('jsonwebtoken');
var express = require('express');
var configuration = require('./config');
var app = express();
var port = configuration.server.PORT;
var bodyparser = require('body-parser');


app.use(bodyparser.json());
app.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Methods", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
	if ('OPTIONS' === req.method) {
		// SE RESPONDE SIEMPRE 200 YA QUE LOS NAVEGADORES HACEN UNA PETICION PREVIA A CADA CONSULTA AJAX
		res.send(200);
	} else {
		next();
	}
});
app.use('/api/*', function (req, res, next) { // Se analiza el token para todas las URLs /api/*
	var token = req.headers['authorization'];
	if (!token) {
		req.dataToken = null;
		next();
		console.log("No se envió un token de seguridad");
		return;
	}

	token = token.replace('Bearer ', '');
	console.log("CODIGO DE SEGURIDAD JWT = " + token);
	jwt.verify(token, configuration.jwt.secret, function (err, dataToken) {
		if (err) {
			req.dataToken = null;
			console.log("Token de Seguridad inválido");
			next();
		} else {
			req.dataToken = dataToken;
			console.log("Token de Seguridad Validado");
			next();
		}
	});
});
app.use('/api/secure/*', function (req, res, next) { // Se valida si es que hay token solo en las URLs que lo requieran (/api/secure/*)
	if (req.dataToken === null) {
		res.status(401).send({
			message: 'Token de Seguridad inválido'
		});
		return;
	}
	next();
});

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

const appRoutes = require('./api/routes/todoListRoutes.js')(app);
